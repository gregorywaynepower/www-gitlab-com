---
layout: handbook-page-toc
title: "Affirmative Action Plan Information"
description: "GitLab is committed to our OFCCP compliance in relation to our Affirmative Action Plan."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

GitLab Inc (including GitLab Federal) is categorized as a U.S. federal contractor, and as such, is subject to annual and ongoing affirmative action requirements.

## Why are we an affirmative action employer?

A US employer is subject to US Federal affirmative action regulations if it has 50 or more employees and a Federal contract or subcontract of $50,000 or more. GitLab met those requirements in 2020.

## What is involved as an affirmative action employer?

GitLab is committed to providing a fair, equitable, and non-discriminatory work environment in which all qualified applicants and employees have an equal opportunity to be employed and to advance in their careers. This pertains to all employment actions, including, but not limited to: hiring, promotion, termination, and the accurate recordkeeping of related data and actions.

GitLab Inc is required to make deliberate efforts to recruit and retain women, minorities (see below), protected veterans, and individuals with disabilities to and within our workforce. This includes making reasonable accommodations for individuals with disabilities and for religious practice/observance. Minorities are defined for all US hires as those identifying as the following ethnicities:

- Asian
- Black/African American
- Hawaiian/Pacific Islander
- Hispanic/Latino
- Indigenous Peoples/Alaska Native
- Two or more races

GitLab Inc must create and maintain a written affirmative action plan (AAP), developed and maintained on an annual basis, which documents our efforts and goals.

GitLab Inc must communicate this context and information to any team member who may as part of their role interview, hire, or promote US-based candidates or team members, even if they themselves are not based in the US.

## What is an affirmative action plan (AAP)?

An affirmative action plan contains both narrative and statistical components, related to any employment action related to a US-based applicant or team member only. Our AAP establishes goals for minority and female placements by job type, as well as a hiring benchmark for protected veterans and a utilization goal for individuals with disabilities.

Our compliance is measured by our efforts to meet each of these goals rather than whether we actually achieve the goals. Note that our AAP does not include quotas and it is unlawful for us to grant preferential treatment based on gender, race, protected veteran, or disability status. Our responsibility is to remove barriers to employment that members of these four groups have historically faced and, in all instances, to select the most qualified individual for the position being filled. The AAP serves as a management tool designed to help us remove those barriers and ensure equal opportunity.

## What are our AAP compliance goals for the US workforce?

To understand our goals, we must look at utilization analysis, which is defined as a tool used in affirmative action plans to compare the demographics of current employees with demographics of the available workforce. 

The goal is to ensure equal access and opportunity for all workers. Placement goals serve as targets or objectives reasonably attainable by applying good faith efforts to our AAP. This number is not a quota. In all employment decisions, selections must be made in a nondiscriminatory manner.

GitLab is striving to enhance employment opportunities for women and minorities throughout our organization. Our annual goals are shared with relevant leaders and team members during our annual AAP training.

## What are our responsibilities?

We all share responsibility for maintaining compliance with equal opportunity and affirmative action laws. The People Compliance team has a responsibility for ensuring systems and procedures are in place to effectively implement our AAP.

Managers, leaders, and recruiting team members have responsibilities under our AAP. These include:

- Ensuring that all interviews and job offers adhere to company policy.
- Administering compensation in accordance with company policy.
- Providing necessary training to employees to enhance personal growth and promotion opportunities.
- Promoting and/or transferring employees in a non-discriminatory manner, awarding each available job to the most qualified candidate.
- Seeking and sharing information on accommodations that have been or could be made for persons with disabilities.
- Protecting applicants and employees from harassment, threats, coercion, intimidation, interference, or discrimination for filing a complaint, assisting in an investigation, or exercising their rights under equal opportunity and affirmative action regulations.
- Ensuring that company-sponsored events are inclusive.

Please note that managers, leaders, and recruiting team members have no metric-based action required regarding our specific AAP compliance goals. As a global company, GitLab strives to work on our global, company-wide hiring and diversity goals, and the US-specific data and success of AAP goals is tracked annually per US law.

If you have questions regarding our affirmative action efforts, the People Compliance Partner is available to assist you and may be reached at `people-compliance@gitlab.com`.
