---
layout: handbook-page-toc
title: "Talent Acquisition Meeting Cadence"
description: "The Talent Acquisition team has a regular schedule of meetings. Please see below for information about cadence and attendance."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Leadership Groups


| **GROUP**                                         | **INDIVIDUALS**                                                                                             |
|-----------------------------------------------|---------------------------------------------------------------------------------------------------------|
| TALT <br>(Talent Acquisition Leadership Team) | Robert Allen, Devin Rogozinski, Jessica Dallmar, Jake Foster, Ursela Knezevic, Marissa Ferber  |
| Manager+                                      | Above group + Ale Ayala, Paul Hardy, Debbie Harris, Rich Kahn, Steph Sarff, Justin Smith |
| Business Leadership                           | Jake Foster, Debbie Harris, Steph Sarff                                                                |
| R+D Leadership                                | Ursela Knezevic, Paul Hardy                                                                             |


## Team Meetings


| Cadence   | Meeting                      | Owner                                         | Content Focus + Goals                                                                                                                                                                                                                                                                                                 |
|-----------|------------------------------|-----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| WEEKLY    | TALT Meeting                 | Jessica Dallmar/Devin Rogozinski              | Senior Leadership: <br>Alignment across our functions<br><br>Big picture TA evolution <br><br>Set the direction for manager+                                                                                                                                                                                          |
| WEEKLY    | CRO Recruiting Review        | Jake Foster                                   | Pipeline health - line by line recruitment updates - opportunity for leadership to ensure team wide progress                                                                                                                                                                                                          |
| WEEKLY    | R&D Recruiting Team Stand-Up | Ursela Knezevic                               | Pipeline health - line by line recruitment updates - opportunity for leadership to ensure team wide progress                                                                                                                                                                                                          |
| WEEKLY    | CES Weekly Meetings          | Ale Ayala                                     | Weekly Operational Updates<br>Announcements<br>Team Problem Solving: Brainstorming spaces for unpacking problems together                                                                                                                                                                                             |
| BI-WEEKLY | Manager+ Meeting             | Jessica Dallmar/Devin Rogozinski              | Drive awareness around team trends and clarity on needs, asks of leaders.<br>Get us all moving in the same direction - clarity on prioritized work, evolution and changes in a timely fashion that supports cohesion between Enablement - Functions and all altitudes of our leadership team.<br>Cascade of feedback. |
| BI-WEEKLY | Global TA Office Hours       | Jake Foster/ Ursela Knezevic                  | 2x monthly meeting to triage offer letter issues and answer questions in a small-group setting.                                                                                                                                                                                                                       |
| MONTHLY   | Global TA Team Meeting       | Jessica Dallmar/Devin Rogozinski/Robert Allen | Quarter to Date: Progress to Goal<br>Announcements<br>TA Strategy Updates / Presentations<br>Team Spotlights<br>Break-outs / Team Building                                                                                                                                                                            |
